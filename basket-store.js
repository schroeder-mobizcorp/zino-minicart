(function() {
	'use strict';

	var basket = {};

	Zino.on('customer-loggedin', function() {
		axios.post('baskets')
		.then(function (response) {
			basket = response.data;
		})
		.catch(function (error) {
			console.log(error);
		});
	});

	Zino.on('add-product-to-basket', function(productId) {
		if(basket) {
			axios.post('baskets/' + basket.basket_id + '/items', {
				'product_id': productId,
				'quantity': 1.00
			})
			.then(function (response) {
				basket = response.data;
				Zino.trigger('basket-updated', basket);
			})
			.catch(function (error) {
				var repsonse = error.response;
				if (repsonse) {
					if(repsonse.status === 400) {
						alert(repsonse.data.fault.message);
					}
				}
			});	
		}
	});
}());