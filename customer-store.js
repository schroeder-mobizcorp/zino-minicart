(function() {
	'use strict';

	window.session = {};
	
	axios.post('customers/auth', {
		type: 'guest'
	})
	.then(function (response) {
		axios.defaults.headers.common['Authorization'] = response.headers.authorization;
		Zino.trigger('customer-loggedin');
	})
	.catch(function (error) {
		console.log(error);
	});
}());