(function() {
	'use strict';

	var products = {};

	Zino.on('get-product', function(productID) {
		axios.get('products' + '/' + productID + '?expand=images,variations')
		.then(function (response) {
			products[response.data.id] = response.data;
			Zino.trigger('product-received', products);
		})
		.catch(function (error) {
			console.log(error);
		});
	});
}());