'use strict';

module.exports = function(grunt) {
	// Project configuration.
	require('load-grunt-tasks')(grunt);

	grunt.initConfig({
		chokidar: {
			options: {
				livereload: true,
				spawn: false
			},
			minicart: {
				files: ['src/*.html'],
				tasks: ['zino']
			},
			zino: {
				files: ['./src/*.js'],
				tasks: ['build']
			}
		},
		connect: {
			server: {
				options: {
					port: 8000,
					base: './',
					open: 'http://localhost:8000/index.html'
				}
			}
		},
		zino: {
			comps: {
				files: [{
					expand: true,
					cwd: './src',
					src: ['*.html'],
					dest: './dist'
				}]
			}
		}
	});

	grunt.registerTask('default', ['zino', 'connect', 'chokidar']);
};